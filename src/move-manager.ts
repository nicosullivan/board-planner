import { AbstractMesh, Scene, Camera, Vector3, Axis, KeyboardInfo, KeyboardEventTypes, Angle, Space, GizmoManager } from "@babylonjs/core";

enum Gizmos {
  None,
  Rotation,
  Position,
}

export default class MoveManager {
  private gizmoManager: GizmoManager;
  private startingPoint: Vector3 | null = null;
  private currentMesh: AbstractMesh | null = null;
  private snapping: boolean = false;
  private managedMeshes: Array<AbstractMesh> = [];
  private gizmoState: Gizmos = Gizmos.None;

  constructor(
    private readonly canvas: HTMLCanvasElement, 
    private readonly scene: Scene,
    private readonly ground: AbstractMesh,
    private readonly camera: Camera,
  ) {
    this.gizmoManager = new GizmoManager(scene);
  }

  private getGroundPosition(): Vector3 | null {
    const pickInfo = this.scene.pick(
      this.scene.pointerX, 
      this.scene.pointerY,
      mesh => mesh === this.ground,
    );

    if (pickInfo?.hit) {
      return pickInfo.pickedPoint;
    }

    return null;
  }

  private onPointerDown(event: PointerEvent) {
    if (event.button !== 0) {
      return;
    }

    const pickInfo = this.scene.pick(
      this.scene.pointerX,
      this.scene.pointerY,
      mesh => mesh !== this.ground,
    );
    if (pickInfo?.hit) {
      if (!this.managedMeshes.includes(<AbstractMesh>pickInfo.pickedMesh)) {
        return;
      }
      this.currentMesh = pickInfo.pickedMesh;
      this.startingPoint = this.getGroundPosition();

      if (this.startingPoint) {
        setTimeout(() => this.camera.detachControl(this.canvas), 0);
      }
    }
  }

  private onPointerUp() {
    if (this.startingPoint) {
      this.camera.attachControl(this.canvas, true);
      this.startingPoint = null;
    }
  }

  private onPointerMove(event: PointerEvent) {
    if (!this.startingPoint) {
      return;
    }
    const current = this.getGroundPosition();

    if (!current) {
      return;
    }

    var diff = current.subtract(this.startingPoint);
    this.currentMesh?.position.addInPlace(diff);

    this.startingPoint = current;
  }

  private onKeyboard({ event: { key }, type}: KeyboardInfo) {
    switch (type) {
      case KeyboardEventTypes.KEYDOWN:
        switch (key.toLowerCase()) {
          case "e":
            this.currentMesh?.rotate(Axis.Y, this.getRotationStep().radians(), Space.LOCAL);
            break;
          case "q":
            this.currentMesh?.rotate(Axis.Y.negate(), this.getRotationStep().radians(), Space.LOCAL);
            break;
          case "shift":
            this.snapping = true;
            break;
          case "g":
            this.nextGizmo();
            break;
        }
        break;
      case KeyboardEventTypes.KEYUP:
        switch (key.toLowerCase()) {
          case "shift":
            this.snapping = false;
            break;
        }
        break;
    }
  }

  private getRotationStep(): Angle {
    const degrees: number = this.snapping ? 45 : 10 ;
    return Angle.FromDegrees(degrees);
  }

  private nextGizmo() {
    this.gizmoState += 1;

    this.gizmoManager.rotationGizmoEnabled = false;
    this.gizmoManager.positionGizmoEnabled = false;

    switch (this.gizmoState) {
      case Gizmos.None:
        break;
      case Gizmos.Position:
        this.gizmoManager.positionGizmoEnabled = true;
        break;
      case Gizmos.Rotation:
        this.gizmoManager.rotationGizmoEnabled = true;
        break;
      default:
        this.gizmoState = Gizmos.None;
        break;
    }
  }

  addListeners() {
    this.canvas.addEventListener("pointerdown", (event: PointerEvent) => this.onPointerDown(event), false);
    this.canvas.addEventListener("pointerup", () => this.onPointerUp(), false);
    this.canvas.addEventListener("pointermove", (event: PointerEvent) => this.onPointerMove(event), false);
    this.scene.onKeyboardObservable.add((info: KeyboardInfo) => this.onKeyboard(info));
    this.scene.onDispose = this.removeListeners;
  }

  removeListeners() {
    this.canvas.removeEventListener("pointerdown", this.onPointerDown);
    this.canvas.removeEventListener("pointerup", this.onPointerUp);
    this.canvas.removeEventListener("pointermove", this.onPointerMove);
  }

  manageMesh(mesh: AbstractMesh) {
    this.managedMeshes.push(mesh);
    this.gizmoManager.attachableMeshes = this.managedMeshes;
  }
}