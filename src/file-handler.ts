export default class FileHandler extends EventTarget {
  constructor() {
    super();
    const targetDiv = document.getElementById("root") as HTMLDivElement;

    targetDiv.ondrop = (event: DragEvent) => this.dropHandler(event);
    targetDiv.ondragover = (event: DragEvent) => this.dragOverHandler(event);    
  }

  private dropHandler(event: DragEvent): void {
    event.preventDefault();
    if (event.dataTransfer?.items) {
      for (var i = 0; i < event.dataTransfer.items.length; i++) {
        if (event.dataTransfer.items[i].kind === 'file') {
          let file = event.dataTransfer.items[i].getAsFile();
          console.log("dropped file: " + file.name);
          this.dispatchEvent(new CustomEvent("new-file", { detail: { file } }));
        }
      } 
    }
  }

  private dragOverHandler(event: DragEvent): void {
    event.preventDefault()
  }
}