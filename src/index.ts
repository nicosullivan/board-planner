import { Engine } from "@babylonjs/core/Engines";
import { Scene, SceneLoader } from "@babylonjs/core";
import { Vector3, Color3 } from "@babylonjs/core/Maths/math";
import { ArcRotateCamera } from "@babylonjs/core/Cameras";
import { HemisphericLight } from "@babylonjs/core/Lights";
import { MeshBuilder } from "@babylonjs/core/Meshes";

import { GridMaterial } from "@babylonjs/materials/grid";

// Required side effects to populate the Create methods on the mesh class. Without this, the bundle would be smaller but the createXXX methods from mesh would not be accessible.
import "@babylonjs/materials/normal/normal.vertex" //needed for objects with no materials
import "@babylonjs/loaders/STL"
import { Scale } from "./constants";
import MoveManager from "./move-manager";
import FileHandler from "./file-handler";

// Get the canvas element from the DOM.
const canvas = document.getElementById("renderCanvas") as HTMLCanvasElement;

// Associate a Babylon Engine to it.
const engine = new Engine(canvas);
engine.getRenderingCanvas()

// Create our first scene.
const scene = new Scene(engine);

// This creates and positions a free camera (non-mesh)
const camera = new ArcRotateCamera("mainCamera", 0, 0, 15, new Vector3(0,0,0), scene);
camera.setPosition(new Vector3(0, 50, 50))
camera.lowerBetaLimit = 0.2;
camera.upperBetaLimit = (Math.PI / 2) * 0.99;
camera.lowerRadiusLimit = 10;

camera.attachControl(canvas, true);

// This creates a light, aiming 0,1,0 - to the sky (non-mesh)
const light = new HemisphericLight("light1", new Vector3(0, 1, 0), scene);

// Default intensity is 1. Let's dim the light a small amount
light.intensity = 0.7;

const ground = MeshBuilder.CreateGround("ground1", { width: 72, height: 36, subdivisions: 2 }, scene);
// Create a grid material
const grid = new GridMaterial("grid", scene);
grid.mainColor = Color3.Green();
grid.lineColor = Color3.Black();
grid.gridRatio = 6;
// Affect a material
ground.material = grid;

const moveManager = new MoveManager(canvas, scene, ground, camera);
moveManager.addListeners();

const fileHandler = new FileHandler();
fileHandler.addEventListener("new-file", ({ detail: { file } }: any) => {
  console.log("handling:", file.name) 
  SceneLoader.ImportMeshAsync("", "/", file, scene).then(result => {
    const mesh = result.meshes[0];
    mesh.scaling.scaleInPlace(Scale.fromMM);
    moveManager.manageMesh(mesh);

    console.log("completed: ", file.name);
  })
})

// Render every frame
engine.runRenderLoop(() => {
  scene.render();
});

window.addEventListener("resize", () => engine.resize());