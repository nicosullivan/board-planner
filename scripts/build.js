import esbuild from "esbuild";
import commandLineArgs from "command-line-args";
import browserSync from "browser-sync";

const { dev, serve } = commandLineArgs([{name: "dev", type: Boolean}, {name: "serve", type: Boolean}]);

const build = await esbuild.build({
  entryPoints: ["src/index.ts"],
  sourcemap: dev,
  incremental: serve,
  outdir: "public/js",
  bundle: true,
}).catch(err => {
  console.error(err);
  process.exit(1);
})

if (serve) {
  const bs = browserSync.create();
  bs.init({
    startPath: "/",
    port: 8080,
    logFileChanges: true,
    single: true,
    server: {
      baseDir: "public",
      index: "index.html",
    },
    files: "src/",
  })
  
  bs.watch(["src/"]).on("change", async _ => {
    build.rebuild();
  })
}
